﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateOrderFromSO99
{
    public class OrderRowToPlan
    {
        public string OrderNo { get; set; }
        public string WarehouseNumber { get; set; }
        public string RowNo { get; set; }
    }
}
