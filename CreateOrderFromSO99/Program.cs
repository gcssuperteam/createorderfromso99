﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using GIS_Dto;
using System.Net.Mail;
using System.Net;
using System.Reflection;

namespace CreateOrderFromSO99
{
    public class Program
    {

        static void Main(string[] args)
        {
            try
            {
                settings = new Logininfo();
                WriteToLog("Startar " + DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + Environment.NewLine);
                GarpLogin();
                ConnectGIS();
                if (ReadImportFile())
                {
                    CreateOrders();
                    CreateMailReports();
                }
                EndGarp();
                RemoveOldLogFiles();
                DeleteImportFile();
                WriteToLog("Klar " + DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + Environment.NewLine);
            }
            catch (Exception e)
            {
                Console.Write("Error " + e);
                WriteToLog("Fel " + e.Message);

            }
        }

        public static bool ReadImportFile()
        {
            try
            {
                WriteToLog("Hämtar order från " + settings.FileImport + Environment.NewLine);

                string textline;
                string CompanyNo = "";
                string CompanyNoTransfer = "";

                // Totalt antal rader i filen
                var lineCount = File.ReadAllLines(settings.FileImport).Length;
                if (lineCount > 1)
                {
                    ManufactoringOrder = new List<Order>();
                    PurchaseOrder = new List<Order>();
                    TransferOrder = new List<Order>();
                    CustomerOrder = new List<Order>();

                    for (int i = 0; i < lineCount; i++)
                    {
                        textline = File.ReadLines(settings.FileImport).ElementAt(i);
                        // ORDERTYPE,ITEM,SUPPLIER,LOCATION,AMOUNT,DATE,SCRDATE,SCRLOCATION

                        if (textline.Length > 7)
                        {
                            try
                            {
                                string[] values = textline.Split(';');

                                if (values[3].Length > 2)
                                {
                                    // Bolagsnummer att skapa order i
                                    CompanyNo = Tools.Left(values[3], 3);

                                    if (values[7].Length > 2)
                                    {
                                        CompanyNoTransfer = Tools.Left(values[7], 3);
                                    }

                                    switch (values[0])
                                    {
                                        case "Manufacturing":       // Lägg till extra case "Manufactoring"
                                            Order Man = new Order
                                            {
                                                Type = "Manufactoring",
                                                Company = CompanyNo,
                                                Item = values[1],
                                                Location = Tools.Mid(values[3], 4),
                                                Amount = values[4],
                                                Date = values[5] // ÅÅÅÅ-MM-DD --> ÅÅMMDD
                                            };
                                            ManufactoringOrder.Add(Man);
                                            break;

                                        case "Purchase":
                                            Order Pur = new Order
                                            {
                                                Type = "PurchaseOrder",
                                                Company = CompanyNo,
                                                Item = values[1],
                                                Supplier = values[2],
                                                Location = Tools.Mid(values[3], 4),
                                                Amount = values[4],
                                                Date = values[5] // ÅÅÅÅ-MM-DD --> ÅÅMMDD
                                            };
                                            PurchaseOrder.Add(Pur);
                                            break;

                                        case "Transfer":

                                            // --- Flyttorder mellan bolagen ---
                                            // Anges som transfer i filen men skall bli kundorder och inköpsorder i Garp
                                            if (values[7].Length > 3 && (values[7] == "003-1" && (CompanyNo == "004" || CompanyNo == "005"))) // Flytt från AB till annat bolag
                                            {
                                                // Order för INLEVERANS
                                                Order TransPur = new Order
                                                {
                                                    Type = "TransPur",
                                                    Company = CompanyNo,
                                                    Item = values[1],
                                                    Supplier = settings.SupplierMafi,
                                                    Location = Tools.Mid(values[3], 4),
                                                    Amount = values[4],
                                                    Date = values[5] // ÅÅÅÅ-MM-DD --> ÅÅMMDD
                                                };
                                                PurchaseOrder.Add(TransPur);

                                                // Order för UTTAG
                                                Order TransCus = new Order
                                                {
                                                    Type = "TransCus",
                                                    Company = CompanyNoTransfer,
                                                    Item = values[1],
                                                    Location = CompanyNo, // Spara bolagsnummeret från Location för att ange mottagare (kundnummer)
                                                    Amount = values[4],
                                                    ScrDate = values[6], // ÅÅÅÅ-MM-DD --> ÅÅMMDD
                                                    ScrLocation = Tools.Mid(values[7], 4)
                                                };
                                                CustomerOrder.Add(TransCus);
                                            }
                                            // --- Flyttorder inom bolaget ---
                                            else
                                            {
                                                if (values[7].Length > 3 && CompanyNo == CompanyNoTransfer)
                                                {
                                                    Order Tra = new Order
                                                    {
                                                        Type = "TransferOrder",
                                                        Company = CompanyNo,
                                                        Item = values[1],
                                                        Supplier = values[2],
                                                        Location = Tools.Mid(values[3], 4),
                                                        Amount = values[4],
                                                        Date = values[5], // ÅÅÅÅ-MM-DD --> ÅÅMMDD
                                                        ScrDate = values[6], // ÅÅÅÅ-MM-DD --> ÅÅMMDD
                                                        ScrLocation = Tools.Mid(values[7], 4)
                                                    };
                                                    TransferOrder.Add(Tra);
                                                }
                                            }
                                            break;

                                        default:
                                            break;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                WriteToLog("Fel vid läsning av fil" + Environment.NewLine + e + Environment.NewLine);
                            }
                        }
                    }
                    return true;
                }
                else
                {
                    WriteToLog("Hittade inga order i filen" + Environment.NewLine);
                    return false;
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel vid läsning av importfilen" + Environment.NewLine + e);
                return false;
            }
        }

        public static void CreateOrders()
        {
            try
            {
                CreateListsForMailReports();

                // --- TILLVERKNINGSORDER ---

                if (ManufactoringOrder.Count > 0)
                {
                    WriteToLog("Skapar tillverkningsorder, " + ManufactoringOrder.Count + " st");

                    OrderToPlan = new List<OrderRowToPlan>();

                    // En order per artikel och datum
                    foreach (Order man in ManufactoringOrder)
                    {
                        OrderRowToPlan ortp = CreateManuOrder(man);
                        OrderToPlan.Add(ortp);
                    }

                    WriteToLog(" - Planerar order" + Environment.NewLine);

                    PlanOrders(OrderToPlan);
                }

                // --- INKÖPSORDER ---

                if (PurchaseOrder.Count > 0)
                {
                    WriteToLog("Skapar inköpsorder, " + PurchaseOrder.Count + " st" + Environment.NewLine);

                    if (PurchaseOrder.Any(x => x.Type == "TransPur"))
                    {
                        List<Order> transPur = PurchaseOrder.FindAll(x => x.Type == "TransPur");
                        //CreateTransPurOrdersOLD(transPur);
                        CreateTransPurOrders(transPur);
                    }

                    if (PurchaseOrder.Any(x => x.Type == "PurchaseOrder"))
                    {
                        List<Order> purchaseOrderList = PurchaseOrder.FindAll(x => x.Type == "PurchaseOrder");
                        // En order per artikel och datum
                        CreatePurchaseOrders(purchaseOrderList);
                    }
                }

                // --- KUNDORDER ---

                //if (CustomerOrder.Count > 0)      //KUNDORDER avaktiverat 200207
                //{
                //    WriteToLog("Skapar kundorder, " + CustomerOrder.Count + " st" + Environment.NewLine);

                //    // EN ORDER PER DATUM
                //    //CreateCustomerOrdersOLD(CustomerOrder);
                //    CreateCustomerOrders(CustomerOrder);
                //}

                // --- FLYTTORDER INOM BOLAGET ---

                if (TransferOrder.Count > 0)
                {
                    WriteToLog("Skapar flyttorder, " + TransferOrder.Count + " st (dubbla)" + Environment.NewLine);

                    // En (dubbel) order per datum = NY ORDER SKALL SKAPAS PER NYTT LEVERANSDATUM
                    //CreateTransferOrdersByDate(TransferOrder);        //Låter denna funktion ligga. Sorterar order på datum, men har ännu ingen lösning för att sen dela upp på kund/leverantör
                    CreateTransferOrders(TransferOrder);                //Denna funktion ger en order per orderrad.
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel vid skapning av orders" + Environment.NewLine + e);
            }
        }

        private static void CreateTransferOrders(List<Order> transferOrder)
        {
            List<Order> transferOrderAB = new List<Order>();
            List<Order> transferOrderUS = new List<Order>();
            List<Order> transferOrderCN = new List<Order>();
            int count = 0;

            try
            {
                if (transferOrder.Any(x => x.Company == "003"))
                {
                    transferOrderAB = transferOrder.FindAll(x => x.Company == "003");
                }

                if (transferOrder.Any(x => x.Company == "004"))
                {
                    transferOrderUS = transferOrder.FindAll(x => x.Company == "004");
                }

                if (transferOrder.Any(x => x.Company == "005"))
                {
                    transferOrderCN = transferOrder.FindAll(x => x.Company == "005");
                }

                foreach (Order to in transferOrderAB)
                {
                    CreateTransferOrderFromTO(to);
                    count++;
                }

                foreach (Order to in transferOrderUS)
                {
                    CreateTransferOrderFromTO(to);
                    count++;
                }

                foreach (Order to in transferOrderCN)
                {
                    CreateTransferOrderFromTO(to);
                    count++;
                }

                WriteToLog("Skapade " + count + " st flyttorder genom CreateTransferOrders" + Environment.NewLine);
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateTransferOrdersByDate" + Environment.NewLine + e);
            }
        }

        private static void CreateTransferOrderFromTO(Order to)
        {
            try
            {
                string OrderNoGarpUttag = CreateTransferBaseOrderFromTO(to, true);
                string OrderNoGarpInleverans = CreateTransferBaseOrderFromTO(to, false);

                UpdateTransferBaseOrderFromTO(to, OrderNoGarpUttag, OrderNoGarpInleverans, true);
                UpdateTransferBaseOrderFromTO(to, OrderNoGarpInleverans, OrderNoGarpUttag, false);
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateTransferOrderFromTO" + Environment.NewLine + e);
            }
        }

        private static void UpdateTransferBaseOrderFromTO(Order trans, string orderNo, string orderNoReference, bool uttag)
        {
            try
            {
                OrderHead oh = new OrderHead();
                List<OrderRow> orList = new List<OrderRow>();
                //Order trans = toList[0];
                Customer c = GetCustomerForTransferBaseOrder(trans, uttag);
                oh.OrderNo = orderNo;

                oh.IsDeliveryAddressUnique = true;
                oh.DeliverCustomerName = c.Name;
                oh.DeliverAddress1 = c.Box;
                oh.DeliverAddress2 = c.Street;
                oh.DeliverZipCity = c.ZipCity;
                oh.CountryCode = c.CountryId;

                if (uttag)
                {
                    oh.YoureReference = "MOT: " + orderNoReference + " " + trans.Location;

                    OrderRow or = new OrderRow();

                    or.OrderNo = orderNo;
                    or.ProductNo = trans.Item;
                    or.WarehouseNo = trans.ScrLocation;
                    or.Amount = 0 - Tools.getDecimalFromStr(trans.Amount); // Negativt antal för uttag
                    tblAGL.Find(Tools.fillBlankRight(trans.Item, 13) + trans.ScrLocation);
                    or.Price = Tools.getDecimalFromStr(fldArtLgrVip.Value); // EVENTUELL MÅSTE PRISET UPPDATERAS EFTER ATT ORDERRADEN HAR SKAPATS
                    or.PreferedDeliverDate = GetDateInGarpFormat(trans.ScrDate);

                    orList.Add(or);
                }
                else
                {
                    oh.YoureReference = "MOT: " + orderNoReference + " " + trans.ScrLocation;

                    OrderRow or = new OrderRow();

                    or.OrderNo = orderNo;
                    or.ProductNo = trans.Item;
                    or.WarehouseNo = trans.Location;
                    or.Amount = Tools.getDecimalFromStr(trans.Amount);
                    tblAGL.Find(Tools.fillBlankRight(trans.Item, 13) + trans.Location);
                    or.Price = Tools.getDecimalFromStr(fldArtLgrVip.Value); // EVENTUELL MÅSTE PRISET UPPDATERAS EFTER ATT ORDERRADEN HAR SKAPATS
                    or.PreferedDeliverDate = GetDateInGarpFormat(trans.Date);

                    orList.Add(or);
                }

                //oh.OrderRows = orList;

                if (trans.Company == "004")
                {
                    mOM_US.UpdateOrderHead(oh);
                    mOM_US.UpdateOrderRowList(orList);
                }
                else if (trans.Company == "005")
                {
                    mOM_CN.UpdateOrderHead(oh);
                    mOM_CN.UpdateOrderRowList(orList);
                }
                else
                {
                    mOM_AB.UpdateOrderHead(oh);
                    mOM_AB.UpdateOrderRowList(orList);
                }

                TransferOrderNo.Add(orderNo);   //Lägger till orderNo i lista för mailutskick

            }
            catch (Exception e)
            {
                WriteToLog("Fel i UpdateTransferBaseOrder" + Environment.NewLine + e);
            }
        }

        private static string CreateTransferBaseOrderFromTO(Order trans, bool uttag)
        {
            string result = "";

            try
            {
                OrderHead oh = new OrderHead();

                oh.OrderNo = settings.OrderSerieTransfer;
                oh.OrderType = "9";

                if (uttag)
                {
                    oh.DeliverCustomerNo = settings.CustomerTransfer + trans.ScrLocation;
                    oh.PreferedDeliverDate = GetDateInGarpFormat(trans.ScrDate);         //Styrande datum
                }
                else
                {
                    oh.DeliverCustomerNo = settings.CustomerTransfer + trans.Location;
                    oh.PreferedDeliverDate = GetDateInGarpFormat(trans.Date);
                }

                if (trans.Company == "004")
                {
                    result = mOM_US.AddOrderHead(oh);
                }
                else if (trans.Company == "005")
                {
                    result = mOM_CN.AddOrderHead(oh);
                }
                else
                {
                    result = mOM_AB.AddOrderHead(oh);
                }

                if (!string.IsNullOrEmpty(result))
                {
                    result = result.Replace("\"", "");
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateTransferBaseOrder" + Environment.NewLine + e);
            }

            return result;
        }

        private static void CreateTransferOrdersByDate(List<Order> transferOrder)
        {
            List<List<Order>> toByDateAB = new List<List<Order>>();             //Denna funktion tar ingen hänsyn till kunder... Sorterar enbart på datum, vilket blir fel(?)
            List<List<Order>> toByDateUS = new List<List<Order>>();             //Kolla vidare på https://stackoverflow.com/questions/18468551/how-to-group-a-list-of-lists-by-date-using-linq
            List<List<Order>> toByDateCN = new List<List<Order>>();
            int count = 0;

            try
            {
                if (transferOrder.Any(x => x.Company == "003"))
                {
                    List<Order> transferOrderAB = transferOrder.FindAll(x => x.Company == "003");
                    toByDateAB = transferOrderAB.GroupBy(x => x.ScrDate).Select(y => y.ToList()).ToList();
                }

                if (transferOrder.Any(x => x.Company == "004"))
                {
                    List<Order> transferOrderUS = transferOrder.FindAll(x => x.Company == "004");
                    toByDateUS = transferOrderUS.GroupBy(x => x.ScrDate).Select(y => y.ToList()).ToList();
                }

                if (transferOrder.Any(x => x.Company == "005"))
                {
                    List<Order> transferOrderCN = transferOrder.FindAll(x => x.Company == "005");
                    toByDateCN = transferOrderCN.GroupBy(x => x.ScrDate).Select(y => y.ToList()).ToList();
                }

                foreach (List<Order> toList in toByDateAB)
                {
                    CreateTransferOrder(toList);
                    count++;
                }

                foreach (List<Order> toList in toByDateUS)
                {
                    CreateTransferOrder(toList);
                    count++;
                }

                foreach (List<Order> toList in toByDateCN)
                {
                    CreateTransferOrder(toList);
                    count++;
                }

                WriteToLog("Skapade " + count + " st flyttorder genom CreateTransferOrders" + Environment.NewLine);

            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateTransferOrdersByDate" + Environment.NewLine + e);
            }
        }

        private static void CreateTransferOrder(List<Order> toList)
        {
            try
            {
                string OrderNoGarpUttag = CreateTransferBaseOrder(toList, true);
                string OrderNoGarpInleverans = CreateTransferBaseOrder(toList, false);

                UpdateTransferBaseOrder(toList, OrderNoGarpUttag, OrderNoGarpInleverans, true);
                UpdateTransferBaseOrder(toList, OrderNoGarpInleverans, OrderNoGarpUttag, false);
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateTransferOrder" + Environment.NewLine + e);
            }
        }

        private static void UpdateTransferBaseOrder(List<Order> toList, string orderNo, string orderNoReference, bool uttag)
        {
            try
            {
                if (toList.Count > 0)
                {
                    OrderHead oh = new OrderHead();
                    List<OrderRow> orList = new List<OrderRow>();
                    Order trans = toList[0];
                    Customer c = GetCustomerForTransferBaseOrder(trans, uttag);
                    oh.OrderNo = orderNo;

                    oh.IsDeliveryAddressUnique = true;
                    oh.DeliverCustomerName = c.Name;
                    oh.DeliverAddress1 = c.Box;
                    oh.DeliverAddress2 = c.Street;
                    oh.DeliverZipCity = c.ZipCity;
                    oh.CountryCode = c.CountryId;

                    if (uttag)
                    {
                        oh.YoureReference = "MOT: " + orderNoReference + " " + trans.Location;

                        foreach (Order order in toList)
                        {
                            OrderRow or = new OrderRow();

                            or.OrderNo = orderNo;
                            or.ProductNo = trans.Item;
                            or.WarehouseNo = trans.ScrLocation;
                            or.Amount = 0 - Tools.getDecimalFromStr(trans.Amount); // Negativt antal för uttag
                            tblAGL.Find(Tools.fillBlankRight(trans.Item, 13) + trans.ScrLocation);
                            or.Price = Tools.getDecimalFromStr(fldArtLgrVip.Value); // EVENTUELL MÅSTE PRISET UPPDATERAS EFTER ATT ORDERRADEN HAR SKAPATS
                            or.PreferedDeliverDate = GetDateInGarpFormat(trans.ScrDate);

                            orList.Add(or);
                        }
                    }
                    else
                    {
                        oh.YoureReference = "MOT: " + orderNoReference + " " + trans.ScrLocation;

                        foreach (Order order in toList)
                        {
                            OrderRow or = new OrderRow();

                            or.OrderNo = orderNo;
                            or.ProductNo = trans.Item;
                            or.WarehouseNo = trans.Location;
                            or.Amount = Tools.getDecimalFromStr(trans.Amount);
                            tblAGL.Find(Tools.fillBlankRight(trans.Item, 13) + trans.Location);
                            or.Price = Tools.getDecimalFromStr(fldArtLgrVip.Value); // EVENTUELL MÅSTE PRISET UPPDATERAS EFTER ATT ORDERRADEN HAR SKAPATS
                            or.PreferedDeliverDate = GetDateInGarpFormat(trans.Date);

                            orList.Add(or);
                        }
                    }

                    //oh.OrderRows = orList;

                    if (trans.Company == "004")
                    {
                        mOM_US.UpdateOrderHead(oh);
                        mOM_US.UpdateOrderRowList(orList);
                    }
                    else if (trans.Company == "005")
                    {
                        mOM_CN.UpdateOrderHead(oh);
                        mOM_CN.UpdateOrderRowList(orList);
                    }
                    else
                    {
                        mOM_AB.UpdateOrderHead(oh);
                        mOM_AB.UpdateOrderRowList(orList);
                    }
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel i UpdateTransferBaseOrder" + Environment.NewLine + e);
            }
        }

        private static Customer GetCustomerForTransferBaseOrder(Order trans, bool uttag)
        {
            Customer result = new Customer();
            string loc = "";
            string customerNo;

            try
            {
                if (uttag)
                {
                    loc = trans.Location;
                }
                else
                {
                    loc = trans.ScrLocation;
                }

                customerNo = settings.CustomerTransfer + loc;

                result = mCM_AB.GetCustomerById(customerNo);


                if (trans.Company == "004")
                {
                    result = mCM_US.GetCustomerById(customerNo);
                }
                else if (trans.Company == "005")
                {
                    result = mCM_CN.GetCustomerById(customerNo);
                }
                else
                {
                    result = mCM_AB.GetCustomerById(customerNo);
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel i GetCustomerForTransferBaseOrder" + Environment.NewLine + e);
            }

            return result;
        }

        private static string CreateTransferBaseOrder(List<Order> toList, bool uttag)
        {
            string result = "";

            try
            {
                if (toList.Count > 0)
                {
                    OrderHead oh = new OrderHead();
                    Order trans = toList[0];

                    oh.OrderNo = settings.OrderSerieTransfer;
                    oh.OrderType = "9";

                    if (uttag)
                    {
                        oh.DeliverCustomerNo = settings.CustomerTransfer + trans.ScrLocation;
                        oh.PreferedDeliverDate = GetDateInGarpFormat(trans.ScrDate);         //Styrande datum
                    }
                    else
                    {
                        oh.DeliverCustomerNo = settings.CustomerTransfer + trans.Location;
                        oh.PreferedDeliverDate = GetDateInGarpFormat(trans.Date);
                    }

                    if (trans.Company == "004")
                    {
                        result = mOM_US.AddOrderHead(oh);
                    }
                    else if (trans.Company == "005")
                    {
                        result = mOM_CN.AddOrderHead(oh);
                    }
                    else
                    {
                        result = mOM_AB.AddOrderHead(oh);
                    }
                }

                if (!string.IsNullOrEmpty(result))
                {
                    result = result.Replace("\"", "");
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateTransferBaseOrder" + Environment.NewLine + e);
            }

            return result;
        }

        private static void PlanOrders(List<OrderRowToPlan> OrderToPlan)
        {
            try
            {
                Garp.IMPSProdPlan ProdPlan = GarpApp.MPSProdPlan;
                Garp.IMPSMtrlPlan MtrlPlan = GarpApp.MPSMtrlPlan;

                // Material och produktionsplanera order
                foreach (OrderRowToPlan order in OrderToPlan)
                {
                    tblOGR.Find(Tools.fillBlankRight(order.OrderNo, 6) + Tools.fillBlankLeft(order.RowNo, 3));
                    ProdPlan.Func = "P";
                    ProdPlan.Ordernr = order.OrderNo;
                    ProdPlan.Radnr = order.RowNo;
                    if (tblAGK.Find(fldArtArtikelNr.Value) && !string.IsNullOrEmpty(fldAgkOpLista.Value) && fldAgkOpLista.Value != fldArtArtikelNr.Value)
                    {
                        ProdPlan.OpLista = fldAgkOpLista.Value;
                    }
                    else
                    {
                        ProdPlan.OpLista = fldOrderRadArtNr.Value;
                    }
                    ProdPlan.Run();

                    MtrlPlan.Func = "B";
                    MtrlPlan.Lagernr = order.WarehouseNumber;
                    MtrlPlan.Ordernr = order.OrderNo;
                    MtrlPlan.Radnr = order.RowNo;
                    MtrlPlan.Run();
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel vid planering av orders" + Environment.NewLine + e);
            }
        }

        private static void CreateCustomerOrdersOLD(List<Order> customerOrder)
        {
            List<OrderHead> ohListAB = new List<OrderHead>();
            List<OrderHead> ohListUS = new List<OrderHead>();
            List<OrderHead> ohListCN = new List<OrderHead>();
            List<List<Order>> ohByDateAB = new List<List<Order>>();
            List<List<Order>> ohByDateUS = new List<List<Order>>();
            List<List<Order>> ohByDateCN = new List<List<Order>>();
            List<string> resultList = new List<string>();

            try
            {
                if (customerOrder.Any(x => x.Company == "003"))
                {
                    List<Order> customerOrdersAB = customerOrder.FindAll(x => x.Company == "003");
                    ohByDateAB = customerOrdersAB.GroupBy(x => x.ScrDate).Select(y => y.ToList()).ToList();
                }

                if (customerOrder.Any(x => x.Company == "004"))
                {
                    List<Order> customerOrdersUS = customerOrder.FindAll(x => x.Company == "004");
                    ohByDateUS = customerOrdersUS.GroupBy(x => x.ScrDate).Select(y => y.ToList()).ToList();
                }

                if (customerOrder.Any(x => x.Company == "005"))
                {
                    List<Order> customerOrdersCN = customerOrder.FindAll(x => x.Company == "005");
                    ohByDateCN = customerOrdersCN.GroupBy(x => x.ScrDate).Select(y => y.ToList()).ToList();
                }

                foreach (List<Order> ohList in ohByDateAB)
                {
                    OrderHead oh = CreateCustomerOrderOLD(ohList);
                    ohListAB.Add(oh);
                }

                foreach (List<Order> ohList in ohByDateUS)
                {
                    OrderHead oh = CreateCustomerOrderOLD(ohList);
                    ohListUS.Add(oh);
                }

                foreach (List<Order> ohList in ohByDateCN)
                {
                    OrderHead oh = CreateCustomerOrderOLD(ohList);
                    ohListCN.Add(oh);
                }

                foreach (OrderHead oh in ohListAB)
                {
                    string result = mOM_AB.AddOrderHead(oh);
                    resultList.Add(result);
                }

                foreach (OrderHead oh in ohListUS)
                {
                    string result = mOM_US.AddOrderHead(oh);
                    resultList.Add(result);
                }

                foreach (OrderHead oh in ohListCN)
                {
                    string result = mOM_CN.AddOrderHead(oh);
                    resultList.Add(result);
                }

                WriteToLog("Skapade " + resultList.Count + " st kundorders genom CreateCustomerOrders" + Environment.NewLine);
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateCustomerOrders" + Environment.NewLine + e);
            }
        }

        private static OrderHead CreateCustomerOrderOLD(List<Order> ohList)
        {
            OrderHead result = new OrderHead();

            try
            {
                List<OrderRow> orList = new List<OrderRow>();

                result.OrderNo = settings.OrderSerieCutomerOrder;
                result.OrderType = "1";
                if (ohList[0].Location == "004")
                {
                    result.DeliverCustomerNo = settings.CustomerMafiUS;
                }
                else if (ohList[0].Location == "005")
                {
                    result.DeliverCustomerNo = settings.CustomerMafiCN;
                }
                result.PreferedDeliverDate = GetDateInGarpFormat(ohList[0].ScrDate);

                foreach (Order cus in ohList)
                {
                    OrderRow or = new OrderRow();

                    or.ProductNo = cus.Item;
                    or.WarehouseNo = cus.ScrLocation;
                    or.Amount = Tools.getDecimalFromStr(cus.Amount);
                    or.PreferedDeliverDate = GetDateInGarpFormat(cus.ScrDate);

                    orList.Add(or);
                }

                result.OrderRows = orList;

            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateCustomerOrder" + Environment.NewLine + e);
            }

            return result;
        }

        private static void CreateCustomerOrders(List<Order> customerOrder)
        {
            //List<OrderHead> ohListAB = new List<OrderHead>();
            //List<OrderHead> ohListUS = new List<OrderHead>();
            //List<OrderHead> ohListCN = new List<OrderHead>();
            List<List<OrderHead>> ohListsAB = new List<List<OrderHead>>();
            List<List<OrderHead>> ohListsUS = new List<List<OrderHead>>();
            List<List<OrderHead>> ohListsCN = new List<List<OrderHead>>();
            List<List<Order>> ohByDateAB = new List<List<Order>>();
            List<List<Order>> ohByDateUS = new List<List<Order>>();
            List<List<Order>> ohByDateCN = new List<List<Order>>();
            List<string> resultList = new List<string>();

            try
            {
                if (customerOrder.Any(x => x.Company == "003"))
                {
                    List<Order> customerOrdersAB = customerOrder.FindAll(x => x.Company == "003");
                    ohByDateAB = customerOrdersAB.GroupBy(x => x.ScrDate).Select(y => y.ToList()).ToList();
                }

                if (customerOrder.Any(x => x.Company == "004"))
                {
                    List<Order> customerOrdersUS = customerOrder.FindAll(x => x.Company == "004");
                    ohByDateUS = customerOrdersUS.GroupBy(x => x.ScrDate).Select(y => y.ToList()).ToList();
                }

                if (customerOrder.Any(x => x.Company == "005"))
                {
                    List<Order> customerOrdersCN = customerOrder.FindAll(x => x.Company == "005");
                    ohByDateCN = customerOrdersCN.GroupBy(x => x.ScrDate).Select(y => y.ToList()).ToList();
                }
                
                foreach (List<Order> ohList in ohByDateAB)
                {
                    ohListsAB.Add(CreateCustomerOrder(ohList));
                }

                foreach (List<Order> ohList in ohByDateUS)
                {
                    ohListsUS.Add(CreateCustomerOrder(ohList));
                }

                foreach (List<Order> ohList in ohByDateCN)
                {
                    ohListsCN.Add(CreateCustomerOrder(ohList));
                }

                foreach (List<OrderHead> oHs in ohListsAB)
                {
                    foreach (var oh in oHs)
                    {
                        string result = mOM_AB.AddOrderHead(oh);
                        resultList.Add(result);
                    }
                }

                foreach (List<OrderHead> oHs in ohListsUS)
                {
                    foreach (var oh in oHs)
                    {
                        string result = mOM_US.AddOrderHead(oh);
                        resultList.Add(result);
                    }
                }

                foreach (List<OrderHead> oHs in ohListsCN)
                {
                    foreach (var oh in oHs)
                    {
                        string result = mOM_CN.AddOrderHead(oh);
                        resultList.Add(result);
                    }
                }

                WriteToLog("Skapade " + resultList.Count + " st kundorders genom CreateCustomerOrders" + Environment.NewLine);
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateCustomerOrders" + Environment.NewLine + e);
            }
        }

        private static List<OrderHead> CreateCustomerOrder(List<Order> ohList)
        {
            List<OrderHead> result = new List<OrderHead>();
            List<Order> ohListUS = new List<Order>();
            List<Order> ohListCN = new List<Order>();
            List<OrderRow> orListUS = new List<OrderRow>();
            List<OrderRow> orListCN = new List<OrderRow>();
            List<List<OrderRow>> orListsUS = new List<List<OrderRow>>();
            List<List<OrderRow>> orListsCN = new List<List<OrderRow>>();

            int orderRowCountUS = 0;
            int orderRowCountCN = 0;

            try
            {
                if (ohList.Any(x => x.Location == "004"))
                {
                    ohListUS = ohList.FindAll(x => x.Location == "004");
                }

                if (ohList.Any(x => x.Location == "005"))
                {
                    ohListCN = ohList.FindAll(x => x.Location == "005");
                }

                foreach (Order order in ohListUS)
                {
                    if (orderRowCountUS > 249)
                    {
                        orListsUS.Add(orListUS);
                        orListUS = new List<OrderRow>();
                        orderRowCountUS = 0;
                    }

                    OrderRow or = new OrderRow();

                    or.ProductNo = order.Item;
                    or.WarehouseNo = order.ScrLocation;
                    or.Amount = Tools.getDecimalFromStr(order.Amount);
                    or.PreferedDeliverDate = GetDateInGarpFormat(order.ScrDate);

                    orListUS.Add(or);
                    orderRowCountUS++;
                }

                orListsUS.Add(orListUS);

                foreach (Order order in ohListCN)
                {
                    if (orderRowCountCN > 249)
                    {
                        orListsCN.Add(orListCN);
                        orListCN = new List<OrderRow>();
                        orderRowCountCN = 0;
                    }

                    OrderRow or = new OrderRow();

                    or.ProductNo = order.Item;
                    or.WarehouseNo = order.ScrLocation;
                    or.Amount = Tools.getDecimalFromStr(order.Amount);
                    or.PreferedDeliverDate = GetDateInGarpFormat(order.ScrDate);

                    orListCN.Add(or);
                    orderRowCountCN++;
                }

                orListsCN.Add(orListCN);

                foreach (List<OrderRow> orList in orListsUS)
                {
                    if (orList.Count == 0)
                    {
                        continue;
                    }

                    OrderHead oh = new OrderHead();
                    oh.OrderNo = settings.OrderSerieCutomerOrder;
                    oh.OrderType = "1";
                    oh.DeliverCustomerNo = settings.CustomerMafiUS;
                    oh.PreferedDeliverDate = orList[0].PreferedDeliverDate; //GetDateInGarpFormat(ohList[0].ScrDate);
                    oh.IsDeliveryAddressUnique = false;

                    oh.OrderRows = orList;

                    result.Add(oh);
                }

                foreach (List<OrderRow> orList in orListsCN)
                {
                    if (orList.Count == 0)
                    {
                        continue;
                    }

                    OrderHead oh = new OrderHead();
                    oh.OrderNo = settings.OrderSerieCutomerOrder;
                    oh.OrderType = "1";
                    oh.DeliverCustomerNo = settings.CustomerMafiCN;
                    oh.PreferedDeliverDate = orList[0].PreferedDeliverDate; //GetDateInGarpFormat(ohList[0].ScrDate);
                    oh.IsDeliveryAddressUnique = false;

                    oh.OrderRows = orList;

                    result.Add(oh);
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateCustomerOrder" + Environment.NewLine + e);
            }

            return result;
        }

        private static OrderRowToPlan CreateManuOrder(Order man)
        {
            OrderRowToPlan result = new OrderRowToPlan();

            try
            {
                OrderHead oh = new OrderHead();
                List<OrderRow> orList = new List<OrderRow>();
                string OrderNoGarp = "";

                oh.OrderNo = settings.OrderSerieManufactoring;
                oh.OrderType = "9";
                oh.DeliverCustomerNo = settings.CustomerManufactoring;
                oh.PreferedDeliverDate = GetDateInGarpFormat(man.Date);

                // Orderrad (en per order)
                OrderRow or = new OrderRow();
                or.ProductNo = man.Item;
                or.WarehouseNo = man.Location;
                or.Amount = Tools.getDecimalFromStr(man.Amount);
                tblAGA.Find(man.Item);
                or.Price = Tools.getDecimalFromStr(fldStdPris.Value); // EVENTUELL MÅSTE PRISET UPPDATERAS EFTER ATT ORDERRADEN HAR SKAPATS
                or.PreferedDeliverDate = GetDateInGarpFormat(man.Date);

                orList.Add(or);
                oh.OrderRows = orList;

                if (man.Company == "004")
                {
                    OrderNoGarp = mOM_US.AddOrderHead(oh);
                }
                else if (man.Company == "005")
                {
                    OrderNoGarp = mOM_CN.AddOrderHead(oh);
                }
                else
                {
                    OrderNoGarp = mOM_AB.AddOrderHead(oh);
                }

                OrderNoGarp = OrderNoGarp.Replace("\"", "");
                //OrderNoGarp = "104386"; // För test

                // Fyll på listan för orderrader att produktionsplanera
                OrderRowToPlan ortp = new OrderRowToPlan
                {
                    OrderNo = OrderNoGarp,
                    WarehouseNumber = man.Location,
                    RowNo = "1" // OBS! OM FLERA ORDERRADER SKALL SKAPAS PER PRODUKTIONSORDER SKALL RADNUMRET OCKSÅ SPARAS I LISTAN FÖR ORDER ATT PLANERA
                };

                ManufactoringOrderNo.Add(OrderNoGarp);      //Lägger till skapat ordernr i Garp i lista för mailutskick.

                result = ortp;
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateManuOrder" + Environment.NewLine + e);
            }

            return result;
        }

        private static void CreateTransPurOrdersOLD(List<Order> purchaseOrder)
        {
            List<PurchaseOrderHead> pOHlistUS = new List<PurchaseOrderHead>();
            List<PurchaseOrderHead> pOHlistCN = new List<PurchaseOrderHead>();
            List<List<Order>> poByDateUS = new List<List<Order>>();
            List<List<Order>> poByDateCN = new List<List<Order>>();
            List<string> resultList = new List<string>();

            try
            {
                if (purchaseOrder.Any(x => x.Company == "004"))
                {
                    List<Order> purchaseOrderUS = purchaseOrder.FindAll(x => x.Company == "004");
                    poByDateUS = purchaseOrderUS.GroupBy(x => x.Date).Select(y => y.ToList()).ToList();
                }

                if (purchaseOrder.Any(x => x.Company == "005"))
                {
                    List<Order> purchaseOrderCN = purchaseOrder.FindAll(x => x.Company == "005");
                    poByDateCN = purchaseOrderCN.GroupBy(x => x.Date).Select(y => y.ToList()).ToList();
                }

                foreach (List<Order> poList in poByDateUS)
                {
                    PurchaseOrderHead pOH = CreateTransPurOrderOLD(poList);
                    pOHlistUS.Add(pOH);
                }

                foreach (List<Order> poList in poByDateCN)
                {
                    PurchaseOrderHead pOH = CreateTransPurOrderOLD(poList);
                    pOHlistCN.Add(pOH);
                }

                foreach (PurchaseOrderHead pOH in pOHlistUS)
                {
                    string result = mPurM_US.AddOrderHead(pOH);
                    resultList.Add(result);
                }

                foreach (PurchaseOrderHead pOH in pOHlistCN)
                {
                    string result = mPurM_CN.AddOrderHead(pOH);
                    resultList.Add(result);
                }

                WriteToLog("Skapade " + resultList.Count + " st inköpsorders genom CreateTransPurOrders" + Environment.NewLine);
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateTransPurOrders" + Environment.NewLine + e);
            }
        }

        private static void CreateTransPurOrders(List<Order> purchaseOrder)
        {
            List<List<PurchaseOrderHead>> pOHlistUS = new List<List<PurchaseOrderHead>>();
            List<List<PurchaseOrderHead>> pOHlistCN = new List<List<PurchaseOrderHead>>();
            List<List<Order>> poByDateUS = new List<List<Order>>();
            List<List<Order>> poByDateCN = new List<List<Order>>();
            List<string> resultList = new List<string>();

            try
            {
                if (purchaseOrder.Any(x => x.Company == "004"))
                {
                    List<Order> purchaseOrderUS = purchaseOrder.FindAll(x => x.Company == "004");
                    poByDateUS = purchaseOrderUS.GroupBy(x => x.Date).Select(y => y.ToList()).ToList();
                }

                if (purchaseOrder.Any(x => x.Company == "005"))
                {
                    List<Order> purchaseOrderCN = purchaseOrder.FindAll(x => x.Company == "005");
                    poByDateCN = purchaseOrderCN.GroupBy(x => x.Date).Select(y => y.ToList()).ToList();
                }

                foreach (List<Order> orderList in poByDateUS)
                {
                    pOHlistUS.Add(CreateTransPurOrder(orderList));
                }

                foreach (List<Order> orderList in poByDateCN)
                {
                    pOHlistCN.Add(CreateTransPurOrder(orderList));
                }

                foreach (List<PurchaseOrderHead> pOHs in pOHlistUS)
                {
                    foreach (var pOH in pOHs)
                    {
                        string result = mPurM_US.AddOrderHead(pOH);
                        result = result.Replace("\"", "");
                        PurchaseUSOrderNo.Add(result);  //Lista för mailutskick
                        resultList.Add(result);
                    }
                }

                foreach (List<PurchaseOrderHead> pOHs in pOHlistCN)
                {
                    foreach (var pOH in pOHs)
                    {
                        string result = mPurM_CN.AddOrderHead(pOH);
                        result = result.Replace("\"", "");
                        PurchaseCNOrderNo.Add(result);  //Lista för mailutskick
                        resultList.Add(result);
                    }
                }

                WriteToLog("Skapade " + resultList.Count + " st inköpsorders genom CreateTransPurOrders" + Environment.NewLine);
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateTransPurOrders" + Environment.NewLine + e);
            }
        }

        private static PurchaseOrderHead CreateTransPurOrderOLD(List<Order> poList)
        {
            PurchaseOrderHead result = new PurchaseOrderHead();

            try
            {
                PurchaseOrderHead pOH = new PurchaseOrderHead();
                List<OrderRow> orList = new List<OrderRow>();
                int orderRowCount = 0;

                pOH.OrderNo = settings.OrderSeriePurchase;
                pOH.OrderType = "1";
                pOH.SupplierNo = poList[0].Supplier;
                pOH.PreferedDeliverDate = GetDateInGarpFormat(poList[0].Date);

                foreach (Order po in poList)
                {
                    OrderRow or = new OrderRow();           //Lägg till koll på antal rader, över 250 så skapa ny order

                    or.ProductNo = po.Item;
                    or.WarehouseNo = po.Location;
                    or.Amount = Tools.getDecimalFromStr(po.Amount);
                    or.PreferedDeliverDate = GetDateInGarpFormat(po.Date);

                    orList.Add(or);
                    orderRowCount++;
                }

                pOH.OrderRows = orList;

                result = pOH;
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateTransPurOrder" + Environment.NewLine + e);
            }

            return result;
        }

        private static List<PurchaseOrderHead> CreateTransPurOrder(List<Order> poList)
        {
            List<PurchaseOrderHead> result = new List<PurchaseOrderHead>();
            List<List<OrderRow>> orderRowsList = new List<List<OrderRow>>();

            try
            {
                List<OrderRow> orList = new List<OrderRow>();
                int orderRowCount = 0;

                foreach (Order po in poList)
                {
                    if (orderRowCount > 249)
                    {
                        orderRowsList.Add(orList);
                        orList = new List<OrderRow>();
                        orderRowCount = 0;
                    }

                    OrderRow or = new OrderRow();

                    or.ProductNo = po.Item;
                    or.WarehouseNo = po.Location;
                    or.Amount = Tools.getDecimalFromStr(po.Amount);
                    or.PreferedDeliverDate = GetDateInGarpFormat(po.Date);

                    orList.Add(or);
                    orderRowCount++;
                }

                orderRowsList.Add(orList);

                foreach (List<OrderRow> orderRowList in orderRowsList)
                {
                    PurchaseOrderHead pOH = new PurchaseOrderHead();

                    pOH.OrderNo = settings.OrderSeriePurchase;
                    pOH.OrderType = "1";
                    pOH.SupplierNo = poList[0].Supplier;
                    pOH.PreferedDeliverDate = GetDateInGarpFormat(poList[0].Date);

                    pOH.OrderRows = orderRowList;

                    result.Add(pOH);
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateTransPurOrder" + Environment.NewLine + e);
            }

            return result;
        }

        private static void CreatePurchaseOrders(List<Order> poList)
        {
            List<string> resultList = new List<string>();

            try
            {
                foreach (var po in poList)
                {
                    PurchaseOrderHead pOH = new PurchaseOrderHead();
                    string result = "";

                    pOH.OrderNo = settings.OrderSeriePurchase;
                    pOH.OrderType = "1";
                    pOH.SupplierNo = po.Supplier;
                    pOH.PreferedDeliverDate = GetDateInGarpFormat(po.Date);

                    // Orderrad (en per order), förutom "flyttorder" från Mafi AB till övriga bolag
                    List<OrderRow> orList = new List<OrderRow>();
                    OrderRow or = new OrderRow();

                    or.ProductNo = po.Item;
                    or.WarehouseNo = po.Location;
                    or.Amount = Tools.getDecimalFromStr(po.Amount);
                    or.PreferedDeliverDate = GetDateInGarpFormat(po.Date);

                    orList.Add(or);
                    pOH.OrderRows = orList;

                    if (po.Company == "004")
                    {
                        //korrekt token och GIS-model
                        result = mPurM_US.AddOrderHead(pOH);
                        result = result.Replace("\"", "");
                        resultList.Add(result);

                        PurchaseUSOrderNo.Add(result);  //För mailutskick, kolla format på strängen!
                    }
                    else if (po.Company == "005")
                    {
                        result = mPurM_CN.AddOrderHead(pOH);
                        result = result.Replace("\"", "");
                        resultList.Add(result);

                        PurchaseCNOrderNo.Add(result);
                    }
                    else
                    {
                        //MafiAB
                        result = mPurM_AB.AddOrderHead(pOH);
                        result = result.Replace("\"", "");
                        resultList.Add(result);

                        PurchaseABOrderNo.Add(result);
                    }
                }

                WriteToLog("Skapade " + resultList.Count + " st inköpsorders genom CreatePurchaseOrders" + Environment.NewLine);
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreatePurchaseOrders" + Environment.NewLine + e);
            }
        }

        private static string GetDateInGarpFormat(string date)
        {
            string result = "";

            try
            {
                if (date.Length > 9)
                {
                    result = date.Replace("-", "");
                    result = result.Substring(2, result.Length - 2);
                }
                else if (date.Length == 6)
                {
                    result = date;
                }
                else
                {
                    result = DateTime.Today.Date.ToString("yyMMdd");    //Om in-strängen är felaktig, så får vi tillbaka dagens datum
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel i GetDateInGarpFormat" + Environment.NewLine + e);
            }

            return result;
        }

        private static void GarpLogin()
        {
            try
            {
                GarpApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GarpApp = new Garp.Application();

                //Loggar in i Garp och sätter bolag, användare och lösen
                WriteToLog("Loggar in i Garp. ");
                if (settings.GarpConfig != null && settings.GarpConfig != "")
                {
                    GarpApp.ChangeConfig(settings.GarpConfig);
                }
                GarpApp.Login(settings.Usr, settings.Password);
                GarpApp.SetBolag(settings.Bolag);

                if (GarpApp.User == null)
                {
                    GarpApp = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    WriteToLog("Kan ej logga in i Garp!" + Environment.NewLine);

                    Environment.Exit(0);
                }

                tblAGA = GarpApp.Tables.Item("AGA");
                fldArtArtikelNr = tblAGA.Fields.Item("ANR");
                fldStdPris = tblAGA.Fields.Item("STP");

                tblAGL = GarpApp.Tables.Item("AGL");
                fldArtLgrVip = tblAGL.Fields.Item("VIP");

                tblOGR = GarpApp.Tables.Item("OGR");
                fldOrderRadArtNr = tblOGR.Fields.Item("ANR");

                tblKA = GarpApp.Tables.Item("KA");
                fldKundNamn = tblKA.Fields.Item("NAM");
                fldKundAdress1 = tblKA.Fields.Item("AD1");
                fldKundAdress2 = tblKA.Fields.Item("AD2");
                fldKundPostnrOrt = tblKA.Fields.Item("ORT");
                fldKundLandskod = tblKA.Fields.Item("LND");

                tblAGK = GarpApp.Tables.Item("AGK");
                fldAgkOpLista = tblAGK.Fields.Item("OP1");

                WriteToLog("Inloggad som " + GarpApp.User + ", terminal " + GarpApp.Terminal + " och bolag " + GarpApp.Bolag + Environment.NewLine);
            }
            catch (Exception e)
            {
                GarpApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                WriteToLog("Kan ej logga in i Garp!" + Environment.NewLine + e + Environment.NewLine + e.Message);

                Environment.Exit(0);
            }
        }

        private static void EndGarp()
        {
            //Stäng Garp
            GarpApp = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            WriteToLog("Stänger Garp" + Environment.NewLine);
        }

        private static void WriteToLog(string text)
        {
            try
            {
                Console.Write(text);
                if (settings.SkrivLoggFil != null)
                {
                    File.AppendAllText(settings.LogFileName, text);
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                if (settings.SkrivLoggFil != null)
                {
                    WriteToLog("Fel uppstod vid skrivning logfil " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
                }
            }
        }
        public static void RemoveOldLogFiles()
        {
            try
            {
                WriteToLog("Raderar logfiler äldre än " + settings.MonthSaveLog + " månader" + Environment.NewLine);
                string[] OldFilses = Directory.GetFiles(Path.GetDirectoryName(settings.LogFileName));
                foreach (string file in OldFilses)
                {
                    FileInfo fileToDelete = new FileInfo(file);
                    if (fileToDelete.CreationTime < DateTime.Now.AddMonths(0 - settings.MonthSaveLog))
                        fileToDelete.Delete();
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel uppstod vid radering gamla logfiler " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
            }
        }

        public static void DeleteImportFile()
        {
            try
            {
                WriteToLog("Tar bort importfil" + Environment.NewLine);

                if (File.Exists(settings.FileImport))
                {
                    File.Delete(settings.FileImport);
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel uppstod vid borttagning av importfil " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
            }
        }

        public static void CreateListsForMailReports()
        {
            try
            {
                ManufactoringOrderNo = new List<string>();
                TransferOrderNo = new List<string>();
                PurchaseABOrderNo = new List<string>();
                PurchaseUSOrderNo = new List<string>();
                PurchaseCNOrderNo = new List<string>();
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateListsForMailReports " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
            }
        }

        public static void CreateMailReports()
        {
            try
            {
                if (ManufactoringOrderNo.Count > 0)
                {
                    string fileName = settings.TempFolderForReport + "\\ManufactoringGarpOrderNo.csv";
                    CreateMail(settings.MailManu, fileName, ManufactoringOrderNo);
                }

                if (TransferOrderNo.Count > 0)
                {
                    string fileName = settings.TempFolderForReport + "\\TransferGarpOrderNo.csv";
                    CreateMail(settings.MailTransfer, fileName, TransferOrderNo);
                }

                if (PurchaseABOrderNo.Count > 0)
                {
                    string fileName = settings.TempFolderForReport + "\\PurchaseABGarpOrderNo.csv";
                    CreateMail(settings.MailPurchaseAB, fileName, PurchaseABOrderNo);
                }

                if (PurchaseUSOrderNo.Count > 0)
                {
                    string fileName = settings.TempFolderForReport + "\\PurchaseUSGarpOrderNo.csv";
                    CreateMail(settings.MailPurchaseUS, fileName, PurchaseUSOrderNo);
                }

                if (PurchaseCNOrderNo.Count > 0)
                {
                    string fileName = settings.TempFolderForReport + "\\PurchaseCNGarpOrderNo.csv";
                    CreateMail(settings.MailPurchaseCN, fileName, PurchaseCNOrderNo);
                }
                
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateMailReports " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
}
        }

        private static void CreateMail(string addresses, string fileName, List<string> report)
        {
            try
            {
                bool attachment = CreateCSVfromErrorReport(report, fileName);

                MailSettings ms = new MailSettings();

                ms.SmtpServer = "smtp.gmail.com";
                ms.SmtpUser = "donotreply@e-com.nu";
                ms.SmtpPassword = "Garp42.tt";
                ms.Port = 587;

                ms.FromName = "No Reply";
                ms.FromAddress = "donotreply@e-com.nu";
                ms.ToName = ms.FromName;
                ms.ToAddress = ms.FromAddress;
                ms.ReplyAddress = "carl-johan.skarstrom@gcsolutions.se";
                ms.BccAddress = addresses.Split(';').ToList();

                ms.Subject = "Created orders in Garp";
                if (attachment)
                {
                    ms.AttachmentPath = fileName;
                    ms.Body = "<b>Created orders in Garp</b><br>Attached file contains a list of created orders in Garp.<br>";
                }
                else
                {
                    ms.Body = "<b>Created orders in Garp</b><br>The logfile could not be attached. Please contact support by reply this mail.<br>";
                }

                SendMail(ms, attachment);
                //mLog.Debug("Mail was sent to " + Addresses);
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateMail " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
            }
        }

        private static bool CreateCSVfromErrorReport(List<string> report, string fileName)
        {
            bool result = false;

            try
            {
                if (report.Count > 0)
                {
                    List<MailCSV> csvList = new List<MailCSV>();

                    foreach (string orderNo in report)
                    {
                        MailCSV csv = new MailCSV();
                        csv.OrderNo = orderNo;
                        csvList.Add(csv);
                    }

                    WriteTextFile(csvList, fileName);
                    result = true;
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel i CreateCSVfromErrorReport " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
            }

            return result;
        }

        private static void SendMail(MailSettings ms, bool attachedFile)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient(ms.SmtpServer);

                // set smtp-client with basicAuthentication
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Port = ms.Port;
                smtpClient.EnableSsl = true;
                NetworkCredential basicAuthenticationInfo = new NetworkCredential(ms.SmtpUser, ms.SmtpPassword);
                smtpClient.Credentials = basicAuthenticationInfo;

                // add from,to mailaddresses
                MailAddress from = new MailAddress(ms.FromAddress, ms.FromName);
                MailAddress to = new MailAddress(ms.ToAddress, ms.ToName);
                MailMessage mail = new MailMessage(from, to);

                foreach (string bcc in ms.BccAddress)
                {
                    mail.Bcc.Add(bcc);
                }

                // add ReplyTo
                mail.ReplyToList.Add(ms.ReplyAddress);

                // set subject and encoding
                mail.Subject = ms.Subject;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                // set body-message and encoding
                mail.Body = ms.Body;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                // text or html
                mail.IsBodyHtml = true;

                if (attachedFile)
                {
                    Attachment attachment = new Attachment(ms.AttachmentPath);
                    mail.Attachments.Add(attachment);
                }

                smtpClient.Send(mail);
            }
            catch (Exception e)
            {
                WriteToLog("Fel i SendMail " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
            }
        }

        public static void WriteTextFile<T>(IEnumerable<T> items, string path)
        {
            Type itemType = typeof(T);
            var props = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            //.OrderBy(p => p.Name);  //Tar bort .OrderBy för att få listan i den ordningen vi har i DTO'n

            Encoding encoding = Encoding.GetEncoding(28591);

            using (var writer = new StreamWriter(path, false, encoding))        //(path)
            {
                writer.WriteLine(string.Join(";", props.Select(p => p.Name)));

                foreach (var item in items)
                {
                    writer.WriteLine(string.Join(";", props.Select(p => p.GetValue(item, null))));
                }
            }
        }

        private static void ConnectGIS()
        {
            try
            {
                tokenAB = settings.TokenAB ?? "";
                tokenUS = settings.TokenUS ?? "";
                tokenCN = settings.TokenCN ?? "";

                mPurM_AB = new GISClientLib.Models.PurchaseModel(tokenAB, true);
                mPurM_US = new GISClientLib.Models.PurchaseModel(tokenUS, true);
                mPurM_CN = new GISClientLib.Models.PurchaseModel(tokenCN, true);
                mOM_AB = new GISClientLib.Models.OrderModel(tokenAB, true);
                mOM_US = new GISClientLib.Models.OrderModel(tokenUS, true);
                mOM_CN = new GISClientLib.Models.OrderModel(tokenCN, true);
                mCM_AB = new GISClientLib.Models.CustomerModel(tokenAB, true);
                mCM_US = new GISClientLib.Models.CustomerModel(tokenUS, true);
                mCM_CN = new GISClientLib.Models.CustomerModel(tokenCN, true);

                mPM = new GISClientLib.Models.ProductModel(tokenAB, true);
                mPM_USA = new GISClientLib.Models.ProductModel(tokenUS, true);
                mBDM = new GISClientLib.Models.BaseDataModel(tokenAB, true);
                mSM = new GISClientLib.Models.SupplierModel(tokenAB, true);
                mSM_USA = new GISClientLib.Models.SupplierModel(tokenUS, true);
                mPRM = new GISClientLib.Models.PrintingModel(tokenAB, true);
            }
            catch (Exception e)
            {
                WriteToLog("Fel uppstod vid anslutning till GIS " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
            }
        }

        static string tokenAB;
        static string tokenUS;
        static string tokenCN;

        static GISClientLib.Models.PurchaseModel mPurM_AB;
        static GISClientLib.Models.PurchaseModel mPurM_US;
        static GISClientLib.Models.PurchaseModel mPurM_CN;
        static GISClientLib.Models.OrderModel mOM_AB;
        static GISClientLib.Models.OrderModel mOM_US;
        static GISClientLib.Models.OrderModel mOM_CN;
        static GISClientLib.Models.CustomerModel mCM_AB;
        static GISClientLib.Models.CustomerModel mCM_US;
        static GISClientLib.Models.CustomerModel mCM_CN;

        static GISClientLib.Models.ProductModel mPM;
        static GISClientLib.Models.ProductModel mPM_USA;
        static GISClientLib.Models.BaseDataModel mBDM;
        static GISClientLib.Models.SupplierModel mSM;
        static GISClientLib.Models.SupplierModel mSM_USA;
        static GISClientLib.Models.PrintingModel mPRM;


        static Garp.Application GarpApp;
        static Garp.ITable tblAGA, tblAGL, tblAGK, tblOGR, tblKA;
        static Garp.ITabField
            fldStdPris

            , fldArtArtikelNr
            , fldArtLgrVip

            , fldOrderRadArtNr

            , fldKundNamn
            , fldKundAdress1
            , fldKundAdress2
            , fldKundPostnrOrt
            , fldKundLandskod

            , fldAgkOpLista
            ;
        static Logininfo settings;
        static List<Order> ManufactoringOrder;
        static List<Order> PurchaseOrder;
        static List<Order> TransferOrder;
        static List<Order> CustomerOrder;
        static List<OrderRowToPlan> OrderToPlan;

        //Listor nedan för mailutskick
        static List<string> ManufactoringOrderNo;
        static List<string> TransferOrderNo;
        static List<string> PurchaseABOrderNo;
        static List<string> PurchaseUSOrderNo;
        static List<string> PurchaseCNOrderNo;
    }

    public class MailSettings
    {
        public string SmtpServer { get; set; }
        public string SmtpUser { get; set; }
        public string SmtpPassword { get; set; }
        public int Port { get; set; }
        public string FromName { get; set; }
        public string FromAddress { get; set; }
        public string ToName { get; set; }
        public string ToAddress { get; set; }
        public string ReplyAddress { get; set; }
        public List<string> BccAddress { get; set; }
        public string Subject { get; set; }
        public string AttachmentPath { get; set; }
        public string Body { get; set; }
    }

    public class MailCSV
    {
        public string OrderNo { get; set; }
    }
}
