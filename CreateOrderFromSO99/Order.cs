﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateOrderFromSO99
{
    public class Order
    {
        public string Type { get; set; }
        public string Company { get; set; }
        public string Item { get; set; }
        public string Supplier { get; set; }
        public string Location { get; set; }
        public string Amount { get; set; }
        public string Date { get; set; }
        public string ScrDate { get; set; }
        public string ScrLocation { get; set; }
    }
}
