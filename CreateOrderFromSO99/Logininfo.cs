﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace CreateOrderFromSO99
{
    public class Logininfo
    {
        public string Bolag { get; set; }
        public string Usr { get; set; }
        public string Password { get; set; }
        public string GarpConfig { get; set; }
        public string SkrivLoggFil { get; set; }
        public string LogFileName { get; set; }
        public string FileImport { get; set; }
        public int MonthSaveLog { get; set; }
        public string FolderImport { get; set; }
        public string OrderSerieManufactoring { get; set; }
        public string OrderSeriePurchase { get; set; }
        public string OrderSerieTransfer { get; set; }
        public string OrderSerieCutomerOrder { get; set; }
        public string CustomerManufactoring { get; set; }
        public string CustomerTransfer { get; set; }
        public string SupplierMafi { get; set; }
        public string CustomerMafiUS { get; set; }
        public string CustomerMafiCN { get; set; }
        public string TokenAB { get; set; }
        public string TokenUS { get; set; }
        public string TokenCN { get; set; }

        //Mailreport
        public string MailTransfer { get; set; }
        public string MailManu { get; set; }
        public string MailPurchaseAB { get; set; }
        public string MailPurchaseUS { get; set; }
        public string MailPurchaseCN { get; set; }
        public string TempFolderForReport { get; set; }


        public Logininfo()
        {
            Bolag = ConfigurationManager.AppSettings["Bolag"];
            Usr = ConfigurationManager.AppSettings["Användare"];
            Password = ConfigurationManager.AppSettings["Lösen"];
            GarpConfig = ConfigurationManager.AppSettings["Konfiguration"];
            if (ConfigurationManager.AppSettings["SkrivLog"] == "True")
            {
                SkrivLoggFil = ConfigurationManager.AppSettings["SkrivLog"];
            }

            LogFileName = string.Format(@"{0}"
            , Path.GetDirectoryName(ConfigurationManager.AppSettings["FilFörLog"])
            + @"\"
            + Path.GetFileNameWithoutExtension(ConfigurationManager.AppSettings["FilFörLog"])
            + " "
            + DateTime.Now.ToString("yyyy-MM-dd HHmm"))
            + Path.GetExtension(ConfigurationManager.AppSettings["FilFörLog"]);

            MonthSaveLog = int.Parse(ConfigurationManager.AppSettings["AntalMånaderSparaLog"]);
            
            FolderImport = string.Format(@"{0}", ConfigurationManager.AppSettings["FolderReceive"]);
            FileImport = FolderImport + string.Format(@"{0}", ConfigurationManager.AppSettings["FileNameReceive"]);

            OrderSerieManufactoring = ConfigurationManager.AppSettings["OrderserieTillverkning"];
            OrderSeriePurchase = ConfigurationManager.AppSettings["OrderserieInköp"];
            OrderSerieTransfer = ConfigurationManager.AppSettings["OrderserieFlytt"];
            OrderSerieCutomerOrder = ConfigurationManager.AppSettings["OrderserieKundorder"];

            CustomerManufactoring = ConfigurationManager.AppSettings["KundnummerTillverkning"];
            CustomerTransfer = ConfigurationManager.AppSettings["KundnummerFlyttorder"];
            SupplierMafi = ConfigurationManager.AppSettings["LeverantörsnummerMafi"];
            CustomerMafiUS = ConfigurationManager.AppSettings["KundnummerMafiUS"];
            CustomerMafiCN = ConfigurationManager.AppSettings["KundnummerMafiCN"];

            TokenAB = ConfigurationManager.AppSettings["tokenAB"];
            TokenUS = ConfigurationManager.AppSettings["tokenUS"];
            TokenCN = ConfigurationManager.AppSettings["tokenCN"];

            //Mailreports
            MailTransfer = ConfigurationManager.AppSettings["mailTransfer"];
            MailManu = ConfigurationManager.AppSettings["mailManu"];
            MailPurchaseAB = ConfigurationManager.AppSettings["mailPurchaseAB"];
            MailPurchaseUS = ConfigurationManager.AppSettings["mailPurchaseUS"];
            MailPurchaseCN = ConfigurationManager.AppSettings["mailPurchaseCN"];
            TempFolderForReport = ConfigurationManager.AppSettings["tempFolderForReport"];
        }
    }
}
